import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { GameMenuComponent } from './game-menu/game-menu.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot()
  ],
  declarations: [
    GameMenuComponent,
  ],
  exports: [
    GameMenuComponent,
  ],
  providers: [
  ]
})
export class ComponentsModule { }

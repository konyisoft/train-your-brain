import { Injectable } from '@angular/core';
import { CardContent } from '../interfaces/card-content';
import { CardTheme } from '../interfaces/card-theme';
import { GridExtent } from '../interfaces/grid-extent';
import * as CardData from '../data/card-data';
import * as GridData from '../data/grid-data';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private currentGridExtent: GridExtent;
  private currentCardTheme: CardTheme;

  private ABC: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  constructor() {
    // Setup theme contents
    this.createSilhouetteImages(CardData.NATURE_IMAGES, CardData.NATURE_IMAGE_PAIRS);
    this.createSilhouetteImages(CardData.OBJECT_IMAGES, CardData.OBJECT_IMAGE_PAIRS);
    this.createUpperCase(this.ABC, CardData.UPPER_CASE);
    this.createLowerCase(this.ABC, CardData.LOWER_CASE);
    this.createUpperLowerCase(this.ABC, CardData.UPPER_LOWER_CASE);
    this.createNumbers(20, CardData.NUMBERS);
    // Default values
    this.GridExtentIndex = 0;
    this.CardThemeIndex = 0;
  }

  set GridExtentIndex(value: number) {
    this.currentGridExtent = GridData.GRID_EXTENTS[value];
  }

  set CardThemeIndex(value: number) {
    this.currentCardTheme = CardData.CARD_THEMES[value];
  }

  get CurrentGridExtent(): GridExtent {
    return this.currentGridExtent;
  }

  get CurrentCardTheme(): CardTheme {
    return this.currentCardTheme;
  }

  private createSilhouetteImages(source: CardContent[], dest: CardContent[]) {
    for (let i = 0; i < source.length; i++) {
      dest.push({
        first: source[i].first,
        second: source[i].first + CardData.CARD_SILHOUETTES_EXTENSION
      });
    }
  }

  private createUpperCase(source: string, dest: CardContent[]) {
    for (let i = 0; i < source.length; i++) {
      dest.push({
        first: source.charAt(i).toUpperCase()
      });
    }
  }

  private createLowerCase(source: string, dest: CardContent[]) {
    for (let i = 0; i < source.length; i++) {
      dest.push({
        first: source.charAt(i).toLowerCase()
      });
    }
  }

  private createUpperLowerCase(source: string, dest: CardContent[]) {
    for (let i = 0; i < source.length; i++) {
      dest.push({
        first: source.charAt(i).toUpperCase(),
        second: source.charAt(i).toLowerCase(),
      });
    }
  }

  private createNumbers(max: number, dest: CardContent[]) {
    for (let i = 0; i <= max; i++) {
      dest.push({ first: '' + i });
    }
  }
}

import { Injectable } from '@angular/core';
import { CardContent } from '../interfaces/card-content';
import { CardTheme } from '../interfaces/card-theme';
import { GridExtent } from '../interfaces/grid-extent';
import { Card, CardType } from '../entities/card';
import { UtilsService } from './utils.service';
import { COLORS } from '../data/misc-data';

@Injectable({
  providedIn: 'root'
})
export class GridService {

  constructor() { }

  createGrid(extent: GridExtent, theme: CardTheme): Card[][] {
    let grid: Card[][] = [];
    let contents: CardContent[] = this.shuffleContents(UtilsService.copyObject(theme.contents)); // clone + shuffle
    let odd: boolean = (extent.rows * extent.cols) % 2 > 0;
    let counter: number = 0; // used as card id
    let content: string;
    let b: boolean = true;
    let options: any;

    for (let row = 0; row < extent.rows; row++) {
      grid[row] = [];
      for (let col = 0; col < extent.cols; col++) {
        // Odd? Create an empty, hidden card with None type.
        if (odd && row == extent.rows - 1 && col == extent.cols - 1) {
          grid[row][col] = new Card(counter, CardType.None, '');
          break;
        }
        // Set content
        if (b) {
          content = contents[counter].first;
          // Randomize colors
          if (theme.options && theme.options.randomColors) {
            options = { textColor: COLORS[UtilsService.random(0, COLORS.length - 1)] };
          }
        } else if (contents[counter].second) {  // is secondary data defined?
          content = contents[counter].second;
        }
        // Create card
        grid[row][col] = new Card(counter, theme.type, content, options);
        b = !b;
        if (b) {
          counter++
        }
      }
    }
    grid = this.shuffleCards(grid);
    return grid;
  }

  private shuffleContents(a: CardContent[]): CardContent[] {
    for (let i = 0; i < a.length; i++) {
      let j: number  = Math.floor(Math.random() * (i + 1));
      let temp: CardContent = a[i];
      a[i] = a[j];
      a[j] = temp;
		}
    return a;
  }

  private shuffleCards(a: Card[][]): Card[][] {
    for (let i = 0; i < a.length; i++) {
      for (let j = 0; j < a[i].length; j++) {
        // Don't touch None type cards
        if (a[i][j].type != CardType.None) {
          let i0: number = Math.floor(Math.random() * (i + 1));
          let j0: number = Math.floor(Math.random() * (j + 1));
          let temp: Card = a[i][j];
          a[i][j] = a[i0][j0];
          a[i0][j0] = temp;
        }
      }
    }
    return a;
  }

}
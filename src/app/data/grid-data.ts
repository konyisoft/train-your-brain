import { GridExtent } from '../interfaces/grid-extent';

export const GRID_EXTENTS: GridExtent[] = [
  { rows: 3, cols: 2, image: '3x2' },  // 6 cards in total
  { rows: 3, cols: 3, image: '3x3' },  // 9
  { rows: 4, cols: 3, image: '4x3' },  // 12
  { rows: 4, cols: 4, image: '4x4' },  // 16
  { rows: 5, cols: 4, image: '5x4' },  // 20
  { rows: 5, cols: 5, image: '5x5' },  // 25
  { rows: 6, cols: 5, image: '6x5' },  // 30
];

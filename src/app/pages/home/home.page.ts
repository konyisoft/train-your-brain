import { Component, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from "@angular/router"
import { GameService } from '../../services/game.service';
import { CardTheme } from '../../interfaces/card-theme';
import { GridExtent } from '../../interfaces/grid-extent';
import * as CardData from '../../data/card-data';
import * as GridData from '../../data/grid-data';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage {

  @ViewChild('gridExtentSlides')
  gridExtentSlides: IonSlides;
  @ViewChild('cardThemeSlides')
  cardThemeSlides: IonSlides;

  gridExtentOptions: any = {
    centeredSlides: true,
    initialSlide: 0,   // 3x2
    loop: true,
    slidesPerView: 3,
  };

  cardThemeOptions: any = {
    centeredSlides: true,
    initialSlide: 0,   // Nature
    loop: true,
    slidesPerView: 3,
  };

  constructor(
    private router: Router,
    public gameService: GameService,
  ) { }

  ionViewWillEnter() {
    this.gridExtentSlides.update();
    this.cardThemeSlides.update();
  }

  onStartGame() {
    this.router.navigate(['/game']);
  }

  onGridExtentChange() {
    this.gridExtentSlides.getSwiper().then((swiper) => {
      this.gameService.GridExtentIndex = swiper.realIndex;
    });
  }

  onCardThemeChange() {
    this.cardThemeSlides.getSwiper().then((swiper) => {
      this.gameService.CardThemeIndex = swiper.realIndex;
    });
  }

  get GridExtents(): GridExtent[] {
    return GridData.GRID_EXTENTS;
  }

  get CardThemes(): CardTheme[] {
    return CardData.CARD_THEMES;
  }
}

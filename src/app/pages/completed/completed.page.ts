import { Component } from '@angular/core';
import { Router } from "@angular/router"
import { TimeService } from '../../services/time.service';

@Component({
  selector: 'app-completed',
  templateUrl: './completed.page.html',
  styleUrls: ['./completed.page.scss'],
})
export class CompletedPage {

  constructor(
    private router: Router,
    public timeService: TimeService
  ) { }

  onBackClick() {
    this.router.navigate(['/'], { replaceUrl: true })
  }
}
